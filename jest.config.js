'use strict'

module.exports = {
    moduleFileExtensions: ['js', 'ts', 'tsx'],
    roots: ['<rootDir>/src/tests'],
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    },
    setupFilesAfterEnv: ['<rootDir>/src/setUpEnzyme.ts'],
    collectCoverage: true,
    collectCoverageFrom: [
        '<rootDir>/src/**/*.{ts,tsx}',
        '!<rootDir>/src/**/*.d.ts',
        '!<rootDir>/src/**/*.spec.ts',
        '!<rootDir>/src/**/*.test.ts',
        '!<rootDir>/src/**/__*__/*',
        '!**/node_modules/**'
    ]
}
