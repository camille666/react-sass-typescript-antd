let registerModule = require('./registerModule.js')
// 添加模块
let demoC = require('../../src/modules/staticDataA/fakeData/index.js')
let demoD = require('../../src/modules/person/fakeData/index.js')
let demoE = require('../../src/modules/common/fakeData/index.js')
let demoA = require('../../src/modules/dynamicDataA/fakeData/index.js')

module.exports = registerModule([demoA, demoC, demoD, demoE])
