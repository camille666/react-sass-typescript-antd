let datas = []
let routes = []
var dataObj = {}
var routeObj = {}

function registerModule(mods = []) {
    for (let item of mods) {
        datas.push(item.datas)
        routes.push(item.routes)
    }
    dataObj = Object.assign(...datas)
    routeObj = Object.assign(...routes)
    console.log({ dataObj, routeObj })
    return { dataObj, routeObj }
}

module.exports = function(mods) {
    return registerModule(mods)
}
