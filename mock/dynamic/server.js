const config = require('./config.js')
const jsonServer = require('json-server')
let registerModule = require('./modules.js')
const rules = registerModule.routeObj
const dbfile = registerModule.dataObj
const ip = config.SERVER
const port = config.PORT

const server = jsonServer.create()
const router = jsonServer.router(dbfile)
const rewriter = jsonServer.rewriter(rules)
const middlewares = jsonServer.defaults()

server.use(jsonServer.bodyParser)
server.use(middlewares)

// 添加响应头
server.use((req, res, next) => {
    res.header('X-Server', 'jsonServer-mockjs')

    if (req.method === 'POST') {
        // req.method = 'GET';
    }
    next()
})

server.use(rewriter)
server.use(router)

server.listen(
    {
        host: ip,
        port: port
    },
    function() {
        console.log(`JSON Server is running in http://${ip}:${port}`)
    }
)
