let registerModule = require('./registerModule.js')
// 添加模块
let demoA = require('../../src/modules/dynamicDataA/fakeData/index.js')
let demoB = require('../../src/modules/common/fakeData/index.js')

module.exports = registerModule([demoA, demoB])
