# react-sass-typescript-antd

## 一、介绍

1、文件结构

````
├── README.md
├── build.gradle
├── doc
│   ├── commitlint.md
│   ├── lintstaged.md
│   ├── precommit.md
│   ├── prettier.md
│   ├── stylelint.md
│   ├── tests.md
│   ├── tsconfig.md
│   └── tslint.md
├── docker
│   ├── Dockerfile
│   ├── nginx.conf
│   └── site.conf
├── jest.config.js
├── mock
│   ├── dynamic
│   └── static
├── package.json
├── src
│   ├── app.tsx
│   ├── assets
│   ├── entries
│   ├── modules
│   ├── setUpEnzyme.ts
│   ├── template
│   ├── tests
│   └── type.d.ts
├── tree.md
├── tsconfig.json
├── tslint.json
├── webpack
│   ├── paths.js
│   ├── webpack.common.config.js
│   ├── webpack.dev.config.js
│   └── webpack.prod.config.js
└── yarn.lock

````

2、架构功能

* 全局的样式文件；
* antd全局注入，按需加载；
* `支持自定义主题文件，进行网站整体换肤`；
* 提取style到不同的css文件，css分割代码，期望的css有main，antd；
* 提取第三方库到对应的js文件，js分割打包，期望的js有第三方公用缓存库，主程序js；
* `脚本调试`；
* 支持通过tsconfig的paths配置，利用别名引用scss文件；
* 支持资源文件复制备份到build目录；
* 开发环境和生产环境的配置区别，代码压缩，实时重载，热更新；
* 测试svg，png，jpg的引入，支持相对路径写死，也支持借助paths别名引入图片模块，并引用；
* 测试icon的用法，支持自定义icon或引用antd的icon；
* 清理webpack编译时输出的无用信息；
* 左侧菜单导航，路由；
* `单模块注册`；
* `mock功能，支持分页，生成随机数据，或者自定义的静态模拟数据，支持mock与业务及框架代码分离，真正会去发请求，而不是拦截请求`；
* `样式代码检查，ts代码检查，提交信息自定义，提交代码的格式化，预提交检查`；
* 工程涉及npm包及工具的选型及使用说明，配置说明；
* 单元测试。


3、一些规范

* 目录命名约束
小写开头，驼峰命名

* 文件命名约束
小写开头，驼峰命名

* 组件类
大写命名

* 提交信息规范
通过工具去约束了。


## 二、一些问题

1、有必要用 editorconfig，eslint，prettier 吗？

https://stackoverflow.com/questions/48363647/editorconfig-vs-eslint-vs-prettier-is-it-worthwhile-to-use-them-all

https://www.npmtrends.com/editorconfig-vs-prettier-vs-prettier-eslint

2、为什么约束命名？

3、一些包的介绍
比如shortid，为什么用？

4、技术选型的考虑
把新的架构搭出来，一个项目统一用一种技术栈。如果要尝试新的技术，可以自己新开工程，基于脚手架去写demo。公司的业务，同一个项目，保持技术栈的统一。技术的多样性和统一不矛盾，都需要。新手维护成本，代码理解成本，老手的迭代跨业务成本。
我们要选稳定的用户群相对比较高的技术或者工具。

`温馨提示`：以上文档及所有代码，会不定期有所更新，或者 bugfix，或者 addfeat。
