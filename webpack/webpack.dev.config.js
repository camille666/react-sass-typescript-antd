/**
 * 实时重载，热模块更新
 * soucemap，方便开发者调试代码
 * 本地服务器
 * 不压缩代码，方便调试
 * css不提取到单独文件
 * 使用sourceMap配置，将源码映射会原始文件（方便调试）
 * 不压缩html
 */
const paths = require('./paths.js')
const path = require('path')
const SOURCE_PATH = paths.SOURCE_PATH

const merge = require('webpack-merge')
const commonWkConfig = require('./webpack.common.config.js')
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const AntdScssThemePlugin = require('antd-scss-theme-plugin')
// 清理webpack编译时输出的无用信息
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
// 控制台报告
const notifier = require('node-notifier')

// loaders
let devLoaders = [
    {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
    },
    {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: '[local]_[hash:base64:5]'
                    }
                }
            },
            AntdScssThemePlugin.themify({
                loader: 'sass-loader'
            })
        ]
    },
    {
        test: /\.less$/,
        use: [
            'style-loader',
            {
                loader: 'css-loader'
            },
            AntdScssThemePlugin.themify({
                loader: 'less-loader',
                options: { javascriptEnabled: true }
            })
        ]
    }
]

// 本地开发 Server 配置
const DEV_SERVER_CONFIG = {
    historyApiFallback: true,
    inline: true,
    progress: true,
    host: '127.0.0.1',
    port: 3008,
    open: true,
    overlay: true,
    quiet: true, // 如果使用webpack-dev-server，需要设为true，禁止显示devServer的console信息
    proxy: {
        '/api/*': {
            // 静态
            target: 'http://127.0.0.1:3005',
            secure: true, // https
            changeOrigin: true // target是域名的话，需要这个参数
            // pathRewrite: { '^/api': '' }
        }
        // '/api/*': { // 动态
        //     target: 'http://127.0.0.1:3003',
        //     secure: true,
        //     changeOrigin: true
        // }
    }
}

// plugins
let devPlugins = [
    new webpack.SourceMapDevToolPlugin({ columns: false }),
    new HtmlWebpackPlugin({
        title: '首页',
        filename: 'index.html',
        template: path.resolve(SOURCE_PATH, './template/index.html'),
        inject: true,
        favicon: path.resolve(SOURCE_PATH, './assets/img/favicon.ico')
    }),
    new FriendlyErrorsWebpackPlugin({
        compilationSuccessInfo: {
            messages: [
                `Your application is running here: http://${DEV_SERVER_CONFIG.host}:${DEV_SERVER_CONFIG.port}`
            ]
        },
        clearConsole: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
]

let devConfig = merge(commonWkConfig, {
    mode: 'development',
    devtool: false,
    module: {
        rules: devLoaders
    },
    plugins: devPlugins,
    devServer: DEV_SERVER_CONFIG
})

module.exports = devConfig
