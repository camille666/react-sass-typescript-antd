import * as React from 'react'

import { Icon, Layout, Menu } from 'antd'
import { BrowserRouter, Route, Switch, withRouter } from 'react-router-dom'

import axios from 'axios'
// const createHistory = require('history').createBrowserHistory;

import rootSubMenuKeys from './modules/common/configs/menuRoot'
const { Header, Sider, Content } = Layout
const MenuItem = Menu.Item
const SubMenu = Menu.SubMenu
import 'styles/base.css'

/**
 * 点击菜单，对应子菜单高亮，刷新右侧内容。
 * 点击按钮，可以伸缩菜单。
 * 支持3级菜单，递归生成菜单。
 * 页面刷新，会保持当前路由对应的菜单高亮。
 */

// 路由列表
const routeList: any[] = []

class AppMenu extends React.Component<any, any> {
    constructor(props: {}) {
        super(props)
        this.state = {
            openKeys: [],
            selectedKeys: location.pathname
        }
    }

    public componentDidMount() {
        const that = this
        axios.get('/api/menuList').then(function(response) {
            that.setState({
                menuList: response.data.data.list || []
            })
        })
    }

    // 递归生成菜单
    public renderMenuItem = (list: any) => {
        const res =
            list &&
            list.length &&
            list.map((route: any) => {
                const childrenList = route.subMenu
                if (childrenList && childrenList.length > 0) {
                    return (
                        <SubMenu
                            key={'/' + route.menuCode}
                            title={
                                <div>
                                    <Icon
                                        type={route.iconCode}
                                        style={{ fontSize: '14px' }}
                                    />
                                    <span>{route.name}</span>
                                </div>
                            }
                        >
                            {this.renderMenuItem(childrenList)}
                        </SubMenu>
                    )
                } else {
                    return (
                        <MenuItem key={'/' + route.menuCode}>
                            <Icon
                                type={route.iconCode}
                                style={{ fontSize: '14px' }}
                            />
                            <span>{route.name}</span>
                        </MenuItem>
                    )
                }
            })
        return res
    }

    // 点击菜单，右边出现对应内容
    public handleMenuClick = (e: any) => {
        const page = e.key
        this.setState({
            selectedKeys: page,
            openKeys: e.keyPath.slice(1)
        })

        // 点击菜单，跳转页面。
        // 这里很容易出现bug，如果之前push错误的路由，后面再怎么写都不对，请重新mock数据。
        this.props.history.push(page)
    }

    // 切换菜单，收起兄弟菜单
    public handleMenuOpenChange = (openKeys: any) => {
        const latestOpenKey = openKeys.find(
            (key: any) => this.state.openKeys.indexOf(key) === -1
        )
        if (rootSubMenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({ openKeys })
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : []
            })
        }
    }

    public render() {
        const { openKeys, selectedKeys, menuList } = this.state

        // 解决收缩时，二级菜单不跟随。
        const openKeysCopy = this.props.collapsed ? [''] : openKeys
        return (
            <Menu
                mode="inline"
                onClick={this.handleMenuClick}
                selectedKeys={[selectedKeys]}
                openKeys={openKeysCopy}
                onOpenChange={this.handleMenuOpenChange}
            >
                {this.renderMenuItem(menuList)}
            </Menu>
        )
    }
}

// App
class App extends React.Component<any, any> {
    public static registePageRoute(moduleRoute: any) {
        if (moduleRoute instanceof Array) {
            routeList.push(...moduleRoute)
        } else {
            routeList.push(moduleRoute)
        }
    }
    constructor(props: {}) {
        super(props)
        this.state = { collapsed: false }
    }

    // 收缩菜单
    public handleMenuCollapsed = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    public render() {
        return (
            <BrowserRouter>
                <Switch>
                    <React.Fragment>
                        <Layout style={{ height: '100%' }}>
                            <Sider
                                style={{ height: '100%' }}
                                trigger={null}
                                collapsible={true}
                                collapsed={this.state.collapsed}
                            >
                                <div className="logo" />
                                <Route
                                    path="/"
                                    render={props => (
                                        <AppMenu
                                            {...props}
                                            collapsed={this.state.collapsed}
                                        />
                                    )}
                                />
                            </Sider>
                            <Layout style={{ height: '100%' }}>
                                <Header
                                    style={{ background: '#fff', padding: 0 }}
                                >
                                    <Icon
                                        className="trigger"
                                        type={
                                            this.state.collapsed
                                                ? 'menu-unfold'
                                                : 'menu-fold'
                                        }
                                        onClick={this.handleMenuCollapsed}
                                    />
                                </Header>
                                <Content
                                    style={{
                                        margin: '24px 16px',
                                        padding: 24,
                                        background: '#fff',
                                        flex: 'none'
                                    }}
                                >
                                    {routeList.map((route: any) => {
                                        return (
                                            <Route
                                                key={route.path}
                                                path={route.path}
                                                component={route.comp}
                                            />
                                        )
                                    })}
                                </Content>
                            </Layout>
                        </Layout>
                    </React.Fragment>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App
