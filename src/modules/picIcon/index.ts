import App from '../../app'
import viewFont from './views/iconFont'
import viewPic from './views/pictures'

App.registePageRoute([
    {
        path: '/iconf',
        comp: viewFont
    },
    {
        path: '/pic',
        comp: viewPic
    }
])
