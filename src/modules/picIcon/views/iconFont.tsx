import { Icon } from 'antd'
import 'fonts/iconfont.js'
import * as React from 'react'

const LocalIcon = Icon.createFromIconfontCN()

export default class IconFont extends React.Component {
    public render() {
        return (
            <div>
                <LocalIcon type="icon-fengxian-qing" />
                <Icon type="smile" />
                <div>
                    <p>1、全局的样式文件； </p>
                    <p>2、antd全局注入，按需加载；</p>
                    <p>3、支持自定义主题文件，进行网站整体换肤；</p>
                    <p>
                        4、提取style到不同的css文件，css分割代码，期望的css有main，antd；
                    </p>
                    <p>
                        5、提取第三方库到对应的js文件，js分割打包；期望的js有第三方公用缓存库，主程序js；
                    </p>
                    <p>
                        6、支持通过tsconfig的paths配置，利用别名引用scss文件；
                    </p>
                    <p>7、支持资源文件复制备份到build目录；</p>
                    <p>
                        8、开发环境和生产环境的配置区别，代码压缩，实时重载，热更新；
                    </p>
                    <p>
                        9、测试svg，png,
                        jpg的引入，支持相对路径写死，也支持借助paths别名引入图片模块，并引用；
                    </p>
                    <p>10、测试icon的用法，支持自定义icon或引用antd的icon；</p>
                    <p>11、清理webpack编译时输出的无用信息。</p>
                    <p>12、学习typescript的写法，加入类型检查。</p>
                </div>
            </div>
        )
    }
}
