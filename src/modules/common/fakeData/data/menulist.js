module.exports = {
    success: true,
    errCode: null,
    tipText: '操作成功',
    errDetail: null,
    data: {
        list: [
            {
                id: 'staffList',
                menuCode: 'staffList',
                name: 'staffList',
                iconCode: 'google'
            },
            {
                id: 'staticDataA',
                menuCode: 'staticDataA',
                name: 'staticDataA',
                iconCode: 'smile'
            },
            {
                id: 'dynamicDataA',
                menuCode: 'dynamicDataA',
                name: 'dynamicDataA',
                iconCode: 'smile'
            },
            {
                id: 'picIcon',
                menuCode: 'picIcon',
                name: 'picIcon',
                iconCode: 'twitter',
                subMenu: [
                    {
                        id: 'iconf',
                        menuCode: 'iconf',
                        name: '图标',
                        iconCode: 'wechat'
                    },
                    {
                        id: 'pic',
                        menuCode: 'pic',
                        name: '图片',
                        iconCode: 'taobao'
                    }
                ]
            },
            {
                id: 'svgCss',
                menuCode: 'svgCss',
                name: 'svgCss',
                iconCode: 'alipay',
                subMenu: [
                    {
                        id: 'cmodule',
                        menuCode: 'cmodule',
                        name: 'css模块',
                        iconCode: 'sketch'
                    },
                    {
                        id: 'svgs',
                        menuCode: 'svgs',
                        name: '矢量',
                        iconCode: 'google'
                    }
                ]
            },
            {
                id: 'oneLayer',
                menuCode: 'oneLayer',
                name: 'oneLayer',
                iconCode: 'smile',
                subMenu: [
                    {
                        id: 'twoLayer',
                        menuCode: 'twoLayer',
                        name: 'twoLayer',
                        iconCode: 'sketch',
                        subMenu: [
                            {
                                id: 'threeLayer',
                                menuCode: 'threeLayer',
                                name: 'threeLayer',
                                iconCode: 'google'
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
