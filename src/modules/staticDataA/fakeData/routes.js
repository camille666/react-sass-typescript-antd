module.exports = {
    '/api/getList*': '/getList',
    '/api/delete*': '/deleteItem',
    '/api/getList\\?id=:id': '/getList/:id',
    '/api/getList/:id': '/getList?id=:id'
}
