module.exports = {
    success: true,
    data: [
        {
            id: '001',
            name: 'banana'
        },
        {
            id: '002',
            name: 'orange'
        },
        {
            id: '003',
            name: 'apple'
        }
    ]
}
