module.exports = {
    getList: require('./data/getList.js'),
    deleteItem: require('./data/deleteItem.js')
}
