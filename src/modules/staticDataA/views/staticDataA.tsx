import * as React from 'react'
import shortid from 'shortid'

class StaticDataA extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            listData: []
        }
    }

    public componentDidMount() {
        const self = this
        const f = fetch('/api/getList')
        f.then(function(response) {
            return response.json()
        }).then(function(data) {
            self.setState({
                listData: data.data
            })
        })
    }

    public render() {
        const { listData } = this.state

        return (
            <div>
                <table>
                    <tbody>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                        </tr>
                        {listData.map((item: any, i: number) => {
                            return (
                                <tr key={shortid.generate()}>
                                    <td>{item.id}</td>
                                    <td>{item.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default StaticDataA
