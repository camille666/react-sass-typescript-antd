import * as React from 'react'
import cStyle from './cmodule.scss'

export default class Cmodule extends React.Component {
    public render() {
        return (
            <div>
                <p className={cStyle.box}>
                    测试cssmodule功能<span>正常</span>
                </p>
                <p className="brother">
                    兄弟节点，没有在配置中额外定义，不会起效。
                </p>
                <p>点击菜单，对应子菜单高亮，刷新右侧内容</p>
                <p>点击按钮，可以伸缩菜单</p>
                <p>页面刷新，会保持当前路由对应的菜单高亮</p>
                <p>支持3级菜单，递归生成菜单。</p>
            </div>
        )
    }
}
