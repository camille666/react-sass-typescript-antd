import * as React from 'react'

export default class Svgs extends React.Component {
    public render() {
        return (
            <embed
                src="./src/assets/img/oppo.svg"
                type="image/svg+xml"
                style={{ transform: 'scale(0.2)' }}
            />
        )
    }
}
