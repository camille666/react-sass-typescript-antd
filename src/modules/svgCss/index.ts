import App from '../../app'
import viewCm from './views/cmodule'
import viewSvg from './views/svgs'

App.registePageRoute([
    {
        path: '/cmodule',
        comp: viewCm
    },
    {
        path: '/svgs',
        comp: viewSvg
    },
    {
        path: '/threeLayer',
        comp: viewSvg
    }
])
