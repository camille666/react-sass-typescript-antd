import { Button, Card, Col, Input, Row, Switch } from 'antd'
import * as React from 'react'
import event from 'ts/events'
import PersonDetail from './components/personDetail'
import PersonList from './components/personList'
import styles from './person.scss'
const { TextArea } = Input
class Person extends React.Component<any, any> {
    public textAreaNode = {}
    constructor(props: {}) {
        super(props)
        this.state = {
            disabled: false,
            remarksDom: ''
        }
    }

    // 展示每项全部信息
    public showAllPersonInfo = (disabled: boolean) => {
        this.setState({
            disabled
        })
    }

    // 添加备注
    public addRemarks = () => {
        const remarks = this.state.remarksDom
        event.emit('ADD_REMARKS', {
            payload: remarks
        })
    }

    // 更新文本域
    public updateTextArea = (e: any) => {
        this.setState({
            remarksDom: e.target.textAreaNode.value
        })
    }

    public render() {
        const { disabled } = this.state
        return (
            <div className={styles.demo}>
                <Card style={{ marginBottom: 15 }}>
                    <h3>
                        展示卡片每项全部信息：
                        <Switch
                            size="small"
                            checked={disabled}
                            onChange={this.showAllPersonInfo}
                        />
                    </h3>
                    <PersonList isShowAll={disabled} />
                </Card>
                <Card>
                    <Row>
                        <Col span={8}>
                            <TextArea
                                ref={(node: any) => {
                                    this.textAreaNode = node
                                }}
                                onChange={this.updateTextArea}
                                rows={4}
                                placeholder="请输入详情备注"
                            />
                        </Col>
                        <Col span={15} offset={1}>
                            <Button type="primary" onClick={this.addRemarks}>
                                添加详情备注
                            </Button>
                        </Col>
                    </Row>
                    <PersonDetail />
                </Card>
            </div>
        )
    }
}

export default Person
