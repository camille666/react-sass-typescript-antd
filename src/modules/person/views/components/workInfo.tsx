import { Table } from 'antd'
import * as React from 'react'
import WorkRemarks from './workRemarks'
class WorkInfo extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
    }

    public render() {
        const { curPersonDetail } = this.props
        const columns = [
            {
                title: '公司名称',
                dataIndex: 'company',
                key: 'company'
            },
            {
                title: '所在职位',
                dataIndex: 'position',
                key: 'position'
            },
            {
                title: '公司规模（/人）',
                dataIndex: 'staffNum',
                key: 'staffNum'
            }
        ]
        return (
            <div>
                <Table
                    pagination={false}
                    columns={columns}
                    dataSource={
                        (curPersonDetail && curPersonDetail.experience) || []
                    }
                    rowKey={(record: any) => record.key}
                />
                <WorkRemarks />
            </div>
        )
    }
}
export default WorkInfo
