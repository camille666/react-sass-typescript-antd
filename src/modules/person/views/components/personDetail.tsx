import { Col, Row } from 'antd'
import * as React from 'react'
import event from 'ts/events'
import styles from './personDetail.scss'
import WorkInfo from './workInfo'
class PersonDetail extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            curPersonDetail: null
        }
    }

    public componentDidMount() {
        event.on('CUR_PERSON_DETAIL', (res: any) => {
            this.setState({
                curPersonDetail: res.payload
            })
        })
    }

    public componentWillUnmount() {
        event.removeListener('CUR_PERSON_DETAIL', (res: any) => {
            this.setState({
                curPersonDetail: res.payload
            })
        })
    }

    public render() {
        const { curPersonDetail } = this.state
        return (
            <div>
                <h3>详情：</h3>
                <div className={styles.personInfo}>
                    <h4>基本信息：</h4>
                    <div className={styles.personBaseInfo}>
                        <Row>
                            <Col span={6}>
                                <p>
                                    <span>Name: </span>
                                    {(curPersonDetail &&
                                        curPersonDetail.name) ||
                                        ''}
                                </p>
                            </Col>
                            <Col span={14} offset={4}>
                                <p>
                                    <span>Age: </span>
                                    {(curPersonDetail && curPersonDetail.age) ||
                                        ''}
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={6}>
                                <p>
                                    <span>Sex: </span>
                                    {(curPersonDetail && curPersonDetail.sex) ||
                                        ''}
                                </p>
                            </Col>
                            <Col span={14} offset={4}>
                                <p>
                                    <span>Weight(/kg): </span>
                                    {(curPersonDetail &&
                                        curPersonDetail.weight) ||
                                        ''}
                                </p>
                            </Col>
                        </Row>
                    </div>
                    <h4>工作经历：</h4>
                    <WorkInfo curPersonDetail={curPersonDetail} />
                </div>
            </div>
        )
    }
}

export default PersonDetail
