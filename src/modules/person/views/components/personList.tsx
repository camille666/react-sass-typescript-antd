import { Card, Row } from 'antd'
import axios from 'axios'
import * as React from 'react'
import shortid from 'shortid'
import event from 'ts/events'
import pStyle from '../person.scss'
class PersonList extends React.Component<any, any> {
    public _isMounted = false

    constructor(props: any) {
        super(props)
        this.state = {
            listData: [],
            curSelected: 0
        }
    }

    public componentDidMount() {
        this._isMounted = true
        const that = this
        axios.get('/api/personList').then(function(response) {
            that.setState(
                {
                    listData: response.data.data || []
                },
                () => {
                    const listData = response.data.data || []
                    event.emit('CUR_PERSON_DETAIL', {
                        payload: (listData.length && listData[0]) || null
                    })
                }
            )
        })
    }

    public componentWillUnmount() {
        this._isMounted = false
    }

    // 点击卡片
    public curCardClick = (item: any, key: any) => {
        this.setState(
            {
                curSelected: key
            },
            () => {
                event.emit('CUR_PERSON_DETAIL', {
                    payload: item
                })
            }
        )
    }

    public render() {
        const { listData, curSelected } = this.state
        const { isShowAll } = this.props
        return (
            <Row>
                {listData &&
                    listData.length &&
                    listData.map((item: any, index: number) => {
                        return (
                            <Card
                                key={shortid.generate()}
                                hoverable={true}
                                className={`${pStyle.card} ${
                                    curSelected === index ? pStyle.active : ''
                                }`}
                                onClick={this.curCardClick.bind(
                                    this,
                                    item,
                                    index
                                )}
                            >
                                <p>{item.name}</p>
                                <p>{item.age}</p>
                                {isShowAll ? (
                                    <div>
                                        <p>{item.sex}</p>
                                        <p>{item.weight}kg</p>
                                    </div>
                                ) : (
                                    '...'
                                )}
                            </Card>
                        )
                    })}
            </Row>
        )
    }
}

export default PersonList
