import * as React from 'react'
import event from 'ts/events'
class WorkRemarks extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            remarks: ''
        }
    }

    public componentDidMount() {
        event.on('ADD_REMARKS', ({ payload }) => {
            this.setState({
                remarks: payload
            })
        })
    }

    public componentWillUnmount() {
        event.removeListener('ADD_REMARKS', ({ payload }) => {
            this.setState({
                remarks: payload
            })
        })
    }

    public render() {
        const { remarks } = this.state
        return (
            <div>
                <p>备注：{remarks}</p>
            </div>
        )
    }
}
export default WorkRemarks
