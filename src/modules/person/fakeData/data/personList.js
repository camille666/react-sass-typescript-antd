module.exports = {
    message: null,
    data: [
        {
            name: 'lily',
            age: 26,
            sex: 'female',
            weight: '50',
            experience: [
                {
                    key: '0',
                    company: 'AA',
                    position: 'HR',
                    staffNum: 500
                },
                {
                    key: '1',
                    company: 'BB',
                    position: 'web',
                    staffNum: 550
                }
            ]
        },
        {
            name: 'sherry',
            age: 30,
            sex: 'female',
            weight: '48',
            experience: [
                {
                    key: '0',
                    company: 'CC',
                    position: 'HR',
                    staffNum: 400
                },
                {
                    key: '1',
                    company: 'DD',
                    position: 'web',
                    staffNum: 420
                }
            ]
        },
        {
            name: 'rose',
            age: 25,
            sex: 'female',
            weight: '51',
            experience: [
                {
                    key: '0',
                    company: 'AA',
                    position: 'HR',
                    staffNum: 600
                },
                {
                    key: '1',
                    company: 'CC',
                    position: 'web',
                    staffNum: 577
                }
            ]
        },
        {
            name: 'zoe',
            age: 22,
            sex: 'female',
            weight: '52',
            experience: [
                {
                    key: '0',
                    company: 'FF',
                    position: 'HR',
                    staffNum: 500
                }
            ]
        },
        {
            name: 'amy',
            age: 12,
            sex: 'female',
            weight: '46',
            experience: []
        },
        {
            name: 'cindy',
            age: 32,
            sex: 'female',
            weight: '55',
            experience: [
                {
                    key: '0',
                    company: 'EE',
                    position: 'HR',
                    staffNum: 500
                },
                {
                    key: '1',
                    company: 'BB',
                    position: 'web',
                    staffNum: 700
                }
            ]
        }
    ],
    success: true
}
