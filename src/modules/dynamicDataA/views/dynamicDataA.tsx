import React from 'react'
import shortid from 'shortid'

class DynamicDataA extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            newsArr: []
        }
    }

    public componentDidMount() {
        const self = this
        const f = fetch('/api/getNewsA?_limit=5')
        f.then(function(response) {
            return response.json()
        }).then(function(data) {
            self.setState({
                newsArr: data
            })
        })
    }

    public render() {
        const { newsArr } = this.state

        return (
            <div style={{ border: '1px solid red' }}>
                {newsArr.map((item: any) => {
                    return (
                        <div key={shortid.generate()}>
                            <p>{item.id}</p>
                            <p>{item.title}</p>
                            <p>{item.desc}</p>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default DynamicDataA
