// id，_limit，_page是json-server内置的，不用声明也能正常使用。
module.exports = {
    '/api/getNewsA*': '/getNewsA',
    '/api/getNewsA\\?id=:id': '/getNewsA/:id',
    '/api/getNewsA/:id': '/getNewsA?id=:id',
    '/api/getNewsA/:_page': '/getNewsA?_page=:_page',
    '/api/getNewsA/:_limit': '/getNewsA?_limit=:_limit',
    '/api/getNewsA\\?_limit=:_limit': '/getNewsA/:_limit'
}
