import * as React from 'react'

interface CheckboxWithLabelProps {
    labelOff: string
    labelOn: string
}

interface CheckboxWithLabelState {
    isChecked: boolean
}

export class CheckboxWithLabel extends React.Component<
    CheckboxWithLabelProps,
    CheckboxWithLabelState
> {
    constructor(props: CheckboxWithLabelProps) {
        super(props)
        this.state = { isChecked: false }
    }

    public onChange = () => {
        this.setState({ isChecked: !this.state.isChecked })
    }

    public render() {
        return (
            <label>
                <input
                    type="checkbox"
                    checked={this.state.isChecked}
                    onChange={this.onChange}
                />
                {this.state.isChecked
                    ? this.props.labelOn
                    : this.props.labelOff}
            </label>
        )
    }
}
