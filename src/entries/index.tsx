import * as React from 'react'
import * as ReactDom from 'react-dom'

import App from '../app'
import '../modules/dynamicDataA'
// import '../modules/person'
import '../modules/picIcon'
import '../modules/staticDataA'
import '../modules/svgCss'

ReactDom.render(<App />, document.getElementById('root'))
