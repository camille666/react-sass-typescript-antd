// 用于跨组件通信
import { EventEmitter } from 'events'
const event = new EventEmitter()
event.setMaxListeners(20)
export default event
