!(function(l) {
    var e,
        n =
            '<svg><symbol id="icon-wenjianjiadakai" viewBox="0 0 1024 1024"><path d="M987.428571 365.714286H950.857143V73.142857H585.142857L512 219.428571H73.142857v146.285715H36.571429c-21.942857 0-36.571429 14.628571-29.257143 36.571428L73.142857 950.857143h877.714286l65.828571-548.571429c7.314286-21.942857-7.314286-36.571429-29.257143-36.571428zM146.285714 292.571429h409.6l21.942857-43.885715 51.2-102.4H877.714286v219.428572H146.285714V292.571429z m746.057143 585.142857H138.971429l-58.514286-438.857143h863.085714l-51.2 438.857143z"  ></path></symbol><symbol id="icon-tingchechang" viewBox="0 0 1024 1024"><path d="M502.698667 0c236.394667 0 428.010667 190.4 427.605333 425.216 0 108.416-41.194667 207.210667-108.586667 282.218667-1.813333 2.986667-3.818667 5.994667-6.4 8.405333L542.464 999.658667a41.6 41.6 0 0 1-8.405333 12.181333c-12.992 13.013333-32.384 15.402667-48.384 8.021333a43.946667 43.946667 0 0 1-14.208-9.408c-1.6-1.6-2.986667-3.605333-4.394667-5.397333L189.077333 715.84c-2.794667-2.986667-5.205333-6.4-6.997333-9.813333C115.477333 631.04 74.666667 533.013333 74.666667 425.024 74.666667 190.4 266.282667 0 502.698667 0z m-145.856 213.504V640h74.666666v-163.669333h113.493334c30.293333 0 54.954667-4.266667 74.090666-12.842667 19.114667-8.554667 33.834667-19.413333 44.202667-32.554667a111.765333 111.765333 0 0 0 21.205333-42.709333 179.008 179.008 0 0 0 5.674667-42.709333c0-13.141333-1.92-27.477333-5.696-43.008a111.573333 111.573333 0 0 0-21.205333-43.008c-10.346667-13.141333-25.066667-24.106667-44.202667-32.853334-19.114667-8.746667-43.797333-13.141333-74.048-13.141333h-188.16z m74.666666 201.898667v-140.970667h110.506667c9.962667 0 19.328 1.002667 28.096 2.986667 8.746667 1.984 16.512 5.674667 23.274667 11.050666 6.784 5.376 12.16 12.650667 16.128 21.802667 3.989333 9.173333 5.973333 20.693333 5.973333 34.645333 0 13.546667-2.282667 24.789333-6.869333 33.749334-4.565333 8.96-10.538667 16.128-17.92 21.504a73.536 73.536 0 0 1-24.192 11.648c-8.746667 2.389333-17.301333 3.584-25.664 3.584h-109.333334z"  ></path></symbol><symbol id="icon-fengxian-qing" viewBox="0 0 1024 1024"><path d="M0 568.888889h256c28.899556 105.187556 117.845333 185.827556 227.555556 199.111111v256C231.367111 1010.261333 27.448889 816.042667 0 568.888889z m0-85.333333C14.279111 223.004444 223.004444 14.279111 483.555556 0v256a261.262222 261.262222 0 0 0-227.555556 227.555556H0z m1024 0h-256c-13.283556-109.710222-93.923556-198.656-199.111111-227.555556V0c247.153778 27.448889 441.372444 231.367111 455.111111 483.555556z m0 85.333333c-26.424889 238.933333-216.177778 428.686222-455.111111 455.111111v-256a287.374222 287.374222 0 0 0 199.111111-199.111111h256z m-512 85.333333a142.222222 142.222222 0 1 1 0-284.444444 142.222222 142.222222 0 0 1 0 284.444444z" fill="#70FCB7" ></path></symbol><symbol id="icon-shuidi" viewBox="0 0 1024 1024"><path d="M514.876 47.173c-172.584 0-312.952 139.793-312.952 312.953 0 106.427 54.076 205.95 143.82 263.478 82.265 52.926 142.094 134.616 169.132 228.962 26.463-93.77 86.293-175.46 168.558-228.386 102.4-65.582 161.078-188.692 139.218-316.405C796.764 157.627 666.75 47.173 514.876 47.173z" fill="#28A8B3" ></path></symbol></svg>',
        t = (e = document.getElementsByTagName('script'))[
            e.length - 1
        ].getAttribute('data-injectcss')
    if (t && !l.__iconfont__svg__cssinject__) {
        l.__iconfont__svg__cssinject__ = !0
        try {
            document.write(
                '<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>'
            )
        } catch (e) {
            console && console.log(e)
        }
    }
    !(function(e) {
        if (document.addEventListener)
            if (
                ~['complete', 'loaded', 'interactive'].indexOf(
                    document.readyState
                )
            )
                setTimeout(e, 0)
            else {
                var t = function() {
                    document.removeEventListener('DOMContentLoaded', t, !1), e()
                }
                document.addEventListener('DOMContentLoaded', t, !1)
            }
        else
            document.attachEvent &&
                ((i = e),
                (o = l.document),
                (c = !1),
                (a = function() {
                    try {
                        o.documentElement.doScroll('left')
                    } catch (e) {
                        return void setTimeout(a, 50)
                    }
                    n()
                })(),
                (o.onreadystatechange = function() {
                    'complete' == o.readyState &&
                        ((o.onreadystatechange = null), n())
                }))
        function n() {
            c || ((c = !0), i())
        }
        var i, o, c, a
    })(function() {
        var e, t
        ;((e = document.createElement('div')).innerHTML = n),
            (n = null),
            (t = e.getElementsByTagName('svg')[0]) &&
                (t.setAttribute('aria-hidden', 'true'),
                (t.style.position = 'absolute'),
                (t.style.width = 0),
                (t.style.height = 0),
                (t.style.overflow = 'hidden'),
                (function(e, t) {
                    t.firstChild
                        ? (function(e, t) {
                              t.parentNode.insertBefore(e, t)
                          })(e, t.firstChild)
                        : t.appendChild(e)
                })(t, document.body))
    })
})(window)
